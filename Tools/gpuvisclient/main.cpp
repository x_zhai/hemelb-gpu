#include <cstdio>
#include <iostream>
#include <memory.h>
#include <GLFW/glfw3.h>

void initGL(GLFWwindow *win, int width, int height);

void initGL(GLFWwindow *win, int width, int height)
{
  int frame_width, frame_height;
  glfwGetFramebufferSize(win, &frame_width, &frame_height);

  glViewport(0,0,frame_width, frame_height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, frame_width, 0.0, frame_height, -10, 10);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  printf("GLFW w %d, h %d\n", frame_width, frame_height);
}

int main (int argc, const char** argv)
{
  int width = 1024;
  int height = 1024;
  unsigned long frame = 0;
  unsigned int* buffer = new unsigned int[width*height];

  memset(buffer, 0, width*height*sizeof(unsigned int));

  if(!glfwInit())
    return -1;

  GLFWwindow* window = glfwCreateWindow(width, height, "hemelb gpu client", NULL, NULL);

  if(!window)
  {
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  initGL(window, width, height);

  while (!glfwWindowShouldClose(window))
  {
    char filename[256];

    snprintf(filename, 256, "%s%08li.dat", argv[1], frame);

    std::FILE* file = std::fopen(filename, "r");

    if(file != NULL)
    {
      while(!std::feof(file))
      {
          unsigned int index;
          int da;
          std::fread(&index, sizeof(unsigned int), 1, file);
          std::fread(&da, sizeof(int), 1, file);

          buffer[index] = da;
      }
      std::fclose(file);
      frame++;
    }
    glClear(GL_COLOR_BUFFER_BIT);

    glRasterPos2i(0,0);
    glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

    glColor3f(1,1,1);
    glBegin(GL_LINES);
    glVertex2i(0, height>>1);
    glVertex2i(width, height>>1);
    glVertex2i(width>>1, 0);
    glVertex2i(width>>1, height);
    glEnd();

    glfwSwapBuffers(window);

    glfwSetWindowTitle(window, filename);

    glfwPollEvents();
  }

  delete [] buffer;

  return 0;
}
