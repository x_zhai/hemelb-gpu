#pragma once

#include <cuda_runtime.h>
#include "extraction/IterableDataSource.h"
#include "net/MpiCommunicator.h"

struct VolumeRendererCUDA
{
public:
  struct float3x4 {
    float4 m[3];
  };

  struct Ray{
    float3 o;
    float3 d;
  };

public:
  VolumeRendererCUDA() {;}
  VolumeRendererCUDA(int width, int height, int volume_sx, int volume_sy, int volume_sz, const hemelb::net::MpiCommunicator* comm);
  ~VolumeRendererCUDA();

  void SlavePreRender(int rank, unsigned long startIteration);

  void MasterPreRender(unsigned long startIteration);

  void Render(unsigned int *target, float density, float brightness, float offset, float scale, int mode);

  void CopyInvViewMatrix(float *invViewMatrix, size_t size);

  void CopyDataToArray(float *data, float *range);

  void Resize(int width, int height);

  void SetDataSource(hemelb::extraction::IterableDataSource* dataSource);

  void SetImageDirectory(const std::string& directory);

  float range[6];

private:

  void freeResources();

  void freeHostResources();

  void InitHostResources(int volume_sx, int volume_sy, int volume_sz);

  void InitRenderer(int width, int height, int volume_sx, int volume_sy, int volume_sz);

  void buildSiteMask();

  void writeImage(const unsigned int* buffer);

  void swapbuffer(uint** lhs, uint** rhs);

  std::string mImageDirectory;
  cudaExtent mVolumeSize;
  dim3 mBlockSize;
  dim3 mGridSize;
  int mImageWidth;
  int mImageHeight;
  int mSystemSize;
  cudaArray *mVolumeArray;
  cudaArray *mSiteMask;
  cudaArray *mTransferFuncArray;
  float* mHostVoxelData;
  float* mHostSiteMaskData;
  uint* mDeviceReferenceBuffer;
  uint* mDeviceRenderBuffer;
  uint* mDeviceDiffBuffer;
  hemelb::extraction::IterableDataSource* mDataSource;
  const hemelb::net::MpiCommunicator* mCommunicator;
  int mSites;
  unsigned long mIterations;
};
