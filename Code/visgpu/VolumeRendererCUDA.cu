#include "VolumeRendererCUDA.cuh"

#include "helper_math.h"
#include "helper_cuda.h"

#include "io/writers/xdr/XdrFileWriter.h"
#include "util/fileutils.h"
#include "HemelbException.h"

__constant__ VolumeRendererCUDA::float3x4 kInvViewMatrix;
__constant__ float4 kValueRange;

texture<float4, cudaTextureType3D, cudaReadModeElementType> mVolumeTex;
texture<float, cudaTextureType3D, cudaReadModeElementType> mSiteMaskTex;
texture<float4, cudaTextureType1D, cudaReadModeElementType> mTransferTex;

__device__ int intersectBox(VolumeRendererCUDA::Ray r, float3 boxmin, float3 boxmax, float *tnear, float *tfar)
{
	// compute intersection of ray with all six bbox planes
    float3 invR = make_float3(1.0f) / r.d;
    float3 tbot = invR * (boxmin - r.o);
    float3 ttop = invR * (boxmax - r.o);

    // re-order intersections to find smallest and largest on each axis
    float3 tmin = fminf(ttop, tbot);
    float3 tmax = fmaxf(ttop, tbot);

    // find the largest tmin and the smallest tmax
    float largest_tmin = fmaxf(fmaxf(tmin.x, tmin.y), fmaxf(tmin.x, tmin.z));
    float smallest_tmax = fminf(fminf(tmax.x, tmax.y), fminf(tmax.x, tmax.z));

    *tnear = largest_tmin;
    *tfar = smallest_tmax;

    return smallest_tmax > largest_tmin;
}

__device__ float3 mul(const VolumeRendererCUDA::float3x4 &M, const float3 &v)
{
    float3 r;
    r.x = dot(v, make_float3(M.m[0]));
    r.y = dot(v, make_float3(M.m[1]));
    r.z = dot(v, make_float3(M.m[2]));
    return r;
}

__device__ float4 mul(const VolumeRendererCUDA::float3x4 &M, const float4 &v)
{
    float4 r;
    r.x = dot(v, M.m[0]);
    r.y = dot(v, M.m[1]);
    r.z = dot(v, M.m[2]);
    r.w = 1.0f;
    return r;
}

__device__ uint rgbaFloatToInt2(float4 rgba)
{
    rgba.x = __saturatef(rgba.x);   // clamp to [0.0, 1.0]
    rgba.y = __saturatef(rgba.y);
    rgba.z = __saturatef(rgba.z);
    rgba.w = __saturatef(rgba.w);
    return (uint(rgba.w*255)<<24) | (uint(rgba.z*255)<<16) | (uint(rgba.y*255)<<8) | uint(rgba.x*255);
}

__global__ void volumeRender_kernel(unsigned int *output, int width, int height, float density, float brightness, float transferOffset, float transferScale, int mode)
{
  const int maxSteps = 500;
  const float tstep = 0.01f;
  const float opacityThreshold = 0.95f;
  const float3 boxMin = make_float3(-1.0f, -1.0f, -1.0f);
  const float3 boxMax = make_float3(1.0f, 1.0f, 1.0f);

  uint x = blockIdx.x*blockDim.x + threadIdx.x;
  uint y = blockIdx.y*blockDim.y + threadIdx.y;

  if ((x >= width) || (y >= height)) return;

  float u = (x / (float) width)*2.0f-1.0f;
  float v = (y / (float) height)*2.0f-1.0f;

  // calculate eye ray in world space
  VolumeRendererCUDA::Ray eyeRay;
  eyeRay.o = make_float3(mul(kInvViewMatrix, make_float4(0.0f, 0.0f, 0.0f, 1.0f)));
  eyeRay.d = normalize(make_float3(u, v, -2.0f));
  eyeRay.d = mul(kInvViewMatrix, eyeRay.d);

  // find intersection with box
  float tnear, tfar;
  int hit = intersectBox(eyeRay, boxMin, boxMax, &tnear, &tfar);

  if (!hit) return;

  if (tnear < 0.0f) tnear = 0.0f;     // clamp to near plane

  // march along ray from front to back, accumulating color
  float4 sum_vel = make_float4(0.0f); //pixel value for velocity component
  float4 sum_pressure = make_float4(0.0f); //pixel value for pressure component
  float4 sum_stress = make_float4(0.0f); //pixel value for stress component

  float t = tnear;
  float3 pos = eyeRay.o + eyeRay.d*tnear;
  float3 step = eyeRay.d*tstep;

  for (int i=0; i<maxSteps; i++)
  {
    // read from 3D texture
    // remap position to [0, 1] coordinates
    float3 pos_in_texcoord = make_float3(pos.x*0.5f + 0.5f, pos.y*0.5f + 0.5f, pos.z*0.5f + 0.5f);
    float4 sample = tex3D(mVolumeTex, pos_in_texcoord.x, pos_in_texcoord.y, pos_in_texcoord.z);
    float mask = tex3D(mSiteMaskTex, pos_in_texcoord.x, pos_in_texcoord.y, pos_in_texcoord.z);
    float3 advection = pos_in_texcoord - make_float3(sample.x, sample.y, sample.z);
    float4 particle = tex3D(mVolumeTex, advection.x, advection.y, advection.z);

    sample.x = (sample.x - kValueRange.x)/(kValueRange.y - kValueRange.x);
    sample.y = (sample.y - 0.0000001f)/0.000002f;
    sample.w = (sample.w - kValueRange.z)/(kValueRange.w - kValueRange.z);

    // lookup in transfer function texture
    float4 col_vel = tex1D(mTransferTex, (sample.x-transferOffset)*transferScale);
    float4 col_stress = tex1D(mTransferTex, (sample.y-transferOffset)*transferScale);
    float4 col_pressure = tex1D(mTransferTex, (sample.w-transferOffset)*transferScale);

    col_vel.w *= density;
    col_pressure.w *= density;
    col_stress.w *= density;

    col_vel.x *= col_vel.w;
    col_vel.y *= col_vel.w;
    col_vel.z *= col_vel.w;

    col_pressure.x *= col_pressure.w;
    col_pressure.y *= col_pressure.w;
    col_pressure.z *= col_pressure.w;

    col_stress.x *= col_stress.w;
    col_stress.y *= col_stress.w;
    col_stress.z *= col_stress.w;

    // "over" operator for front-to-back blending
    sum_vel = sum_vel + mask*col_vel*(1.0f - sum_vel.w);
    sum_pressure = sum_pressure + mask*col_pressure*(1.0f - sum_pressure.w);
    sum_stress = sum_stress + mask*col_stress*(1.0f - sum_stress.w);

    // exit early if opaque
    if (sum_vel.w > opacityThreshold && sum_pressure.w > opacityThreshold && sum_stress.w > opacityThreshold)
        break;

    t += tstep;

    if (t > tfar) break;

    pos += step;
  }
  sum_vel *= brightness;
  sum_pressure *= brightness;
  sum_stress *= brightness;

  //Write each visualised value into its designated quadrant
  //Top-left : pressure
  //Top-Right : Shear Stress
  //Bottom-Right: Velocity
  output[(y>>1)*width + (x>>1) + (width>>1)] = rgbaFloatToInt2(sum_vel);
  output[((y>>1) + (height>>1))*width + (x>>1)] = rgbaFloatToInt2(sum_pressure);
  output[((y>>1) + (height>>1))*width + (x>>1)+(width>>1)] = rgbaFloatToInt2(sum_stress);
}

__global__ void imageDiff_kernel(uint *output, int width, int height, const unsigned int* ref, const unsigned int* input)
{
  uint x = blockIdx.x*blockDim.x + threadIdx.x;
  uint y = blockIdx.y*blockDim.y + threadIdx.y;

  if ((x >= width) || (y >= height)) return;

  uint index = y*width + x;

  output[index] = (input[index] & 0x00FFFFFF) - (ref[index] & 0x00FFFFFF);
}

VolumeRendererCUDA::VolumeRendererCUDA(int width, int height, int volume_sx, int volume_sy, int volume_sz, const hemelb::net::MpiCommunicator* comm)
{
  mCommunicator = comm;
  mSystemSize = volume_sx;
  mIterations = 0;

  if(mCommunicator->Rank() == 0)
  {
    InitHostResources(mSystemSize, mSystemSize, mSystemSize);
    InitRenderer(width, height, volume_sx, volume_sy, volume_sz);
  }
}

VolumeRendererCUDA::~VolumeRendererCUDA()
{
  if(mCommunicator->Rank() == 0)
  {
    freeResources();
    freeHostResources();
  }
}

void VolumeRendererCUDA::SetImageDirectory(const std::string& directory)
{
  mImageDirectory = directory+"/GPUImages/";

  hemelb::util::MakeDirAllRXW(mImageDirectory);
}

void VolumeRendererCUDA::SetDataSource(hemelb::extraction::IterableDataSource* dataSource)
{
  mDataSource = dataSource;

  buildSiteMask();
}

void VolumeRendererCUDA::buildSiteMask()
{
  //temp storage for packing vis data before being sent
  //Extract LB site location from data source
  //Create a mask for fluid sites
  if( mCommunicator->Rank() == 0)
  {
    int nprocs = mCommunicator->Size();

    for(int i = 1; i < nprocs; i++)
    {
      mCommunicator->Send('S', i);
      hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Sent %c request to proc %d.", 'S', i);

      char msg;
      mCommunicator->Receive(msg, i);
      hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Received ack from proc %d.", i);

      size_t expected = 0;
      //siteGlobalIndexStore.resize(expected);
      mCommunicator->Receive(expected, i);
      std::vector<size_t> siteGlobalIndexStore(expected);

      mCommunicator->Receive(siteGlobalIndexStore, i);
      hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Expecting %d sites, received %d site indices from Proc %d.", expected, siteGlobalIndexStore.size(), i);

      int dataSize = siteGlobalIndexStore.size();

      for(int j = 0; j < dataSize; j++)
      {
        mHostSiteMaskData[siteGlobalIndexStore[j]] = 1.0f;
      }
    }

    mDataSource->Reset();

    size_t index;
    while(mDataSource->ReadNext())
    {
      hemelb::util::Vector3D<size_t> sitePos = mDataSource->GetPosition();
      index = (sitePos.z*mSystemSize + sitePos.y)*mSystemSize + sitePos.x;

      mHostSiteMaskData[index] = 1.0f;
    }

    cudaMemcpy3DParms copyParams = {0};
    copyParams.srcPtr = make_cudaPitchedPtr(mHostSiteMaskData, mVolumeSize.width*sizeof(float), mVolumeSize.width, mVolumeSize.height);
    copyParams.dstArray = mSiteMask;
    copyParams.extent   = mVolumeSize;
    copyParams.kind     = cudaMemcpyHostToDevice;
    checkCudaErrors(cudaMemcpy3D(&copyParams));
  }
  else
  {
    //Send a message so that IORank knows this Comm is sending data
    char msg;
    mCommunicator->Receive(msg, 0);
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Received %c message from IORank", msg);
    mCommunicator->Send('R', 0);
    mDataSource->Reset();
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Populating site global index store with the global index of each fluid site.");

    size_t index;
    hemelb::util::Vector3D<size_t> sitePos;
    std::vector<size_t> siteGlobalIndexStore;
    while(mDataSource->ReadNext())
    {
      sitePos = mDataSource->GetPosition();
      index = (sitePos.z*mSystemSize + sitePos.y)*mSystemSize + sitePos.x;
      siteGlobalIndexStore.push_back(index);
    }
    mSites = siteGlobalIndexStore.size();
    mCommunicator->Send(mSites, 0);
    mCommunicator->Send(siteGlobalIndexStore, 0);
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Sending %d site indices to IORank.", siteGlobalIndexStore.size());
  }
}

void VolumeRendererCUDA::Resize(int width, int height)
{
 	mImageWidth = width;
	mImageHeight =height;

	mBlockSize.x = 16;
	mBlockSize.y = 16;
	mBlockSize.z = 1;

	mGridSize.x = mImageWidth % mBlockSize.x == 0 ? mImageWidth/mBlockSize.x : mImageWidth/mBlockSize.x + 1;
	mGridSize.y = mImageHeight % mBlockSize.y == 0 ? mImageWidth/mBlockSize.y : mImageWidth/mBlockSize.y + 1;
  mGridSize.z = 1;

  if(mDeviceReferenceBuffer)
  {
    checkCudaErrors(cudaFree(mDeviceReferenceBuffer));
    checkCudaErrors(cudaMallocManaged(&mDeviceReferenceBuffer, width*height*sizeof(uint)));
    checkCudaErrors(cudaMemset(mDeviceReferenceBuffer, 0, width*height*sizeof(uint)));
  }

  if(mDeviceRenderBuffer)
  {
    checkCudaErrors(cudaFree(mDeviceRenderBuffer));
    checkCudaErrors(cudaMallocManaged(&mDeviceRenderBuffer, width*height*sizeof(uint)));
    checkCudaErrors(cudaMemset(mDeviceRenderBuffer, 0, width*height*sizeof(uint)));
  }

  if(mDeviceDiffBuffer)
  {
    checkCudaErrors(cudaFree(mDeviceDiffBuffer));
    checkCudaErrors(cudaMallocManaged(&mDeviceDiffBuffer, width*height*sizeof(uint)));
    checkCudaErrors(cudaMemset(mDeviceDiffBuffer, 0, width*height*sizeof(uint)));
  }
}

void VolumeRendererCUDA::InitHostResources(int volume_sx, int volume_sy, int volume_sz)
{

  mHostVoxelData = new float[4*mSystemSize*mSystemSize*mSystemSize];

  memset(mHostVoxelData, 0, sizeof(float)*4*mSystemSize*mSystemSize*mSystemSize);

  mHostSiteMaskData = new float[mSystemSize*mSystemSize*mSystemSize];

  memset(mHostSiteMaskData, 0, sizeof(float)*mSystemSize*mSystemSize*mSystemSize);

  mDataSource = NULL;

  //min max for mapping voxel to the colour map
  //range[0] - min velocity magnitude
  //range[1] - max velocity magnitude
  //range[2] - min pressure
  //range[3] - max pressure
  range[0] = 0.0f;
  range[1] = 0.02f;
  range[2] = -0.01f;
  range[3] = 0.2f;

  //range[4] = -10.0f;
  //range[5] = 10.0f;
}

void VolumeRendererCUDA::InitRenderer(int width, int height, int volume_sx, int volume_sy, int volume_sz)
{

	mImageWidth = width;
	mImageHeight =height;

	mBlockSize.x = 16;
	mBlockSize.y = 16;
	mBlockSize.z = 1;

	mGridSize.x = mImageWidth % mBlockSize.x == 0 ? mImageWidth/mBlockSize.x : mImageWidth/mBlockSize.x + 1;
	mGridSize.y = mImageHeight % mBlockSize.y == 0 ? mImageWidth/mBlockSize.y : mImageWidth/mBlockSize.y + 1;
	mGridSize.z = 1;

	 // create 3D volume texture
	mVolumeSize = make_cudaExtent(volume_sx, volume_sy, volume_sz);

  cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();
  checkCudaErrors(cudaMalloc3DArray(&mVolumeArray, &channelDesc, mVolumeSize));

  // set texture parameters
  mVolumeTex.normalized = true;                      // access with normalized texture coordinates
  mVolumeTex.filterMode = cudaFilterModeLinear;      // linear interpolation
  mVolumeTex.addressMode[0] = cudaAddressModeClamp;  // clamp texture coordinates
  mVolumeTex.addressMode[1] = cudaAddressModeClamp;
  // bind array to 3D texture
  checkCudaErrors(cudaBindTextureToArray(mVolumeTex, mVolumeArray, channelDesc));

  cudaChannelFormatDesc channelDesc3 = cudaCreateChannelDesc<float>();
  checkCudaErrors(cudaMalloc3DArray(&mSiteMask, &channelDesc3, mVolumeSize));
  // set texture parameters
  mSiteMaskTex.normalized = true;                      // access with normalized texture coordinates
  mSiteMaskTex.filterMode = cudaFilterModeLinear;      // interpolation
  mSiteMaskTex.addressMode[0] = cudaAddressModeClamp;  // clamp texture coordinates
  mSiteMaskTex.addressMode[1] = cudaAddressModeClamp;
  // bind array to 3D texture
  checkCudaErrors(cudaBindTextureToArray(mSiteMaskTex, mSiteMask, channelDesc3));
  // create transfer function texture

  float4 transferFunc[] =
  {
    {  0.0, 0.0, 1.0, 1.0, },
    {  0.0, 1.0, 1.0, 1.0, },
    {  0.0, 1.0, 0.0, 1.0, },
    {  1.0, 1.0, 0.0, 1.0, },
    {  1.0, 0.0, 0.0, 1.0, },
  };

  cudaChannelFormatDesc channelDesc2 = cudaCreateChannelDesc<float4>();
  checkCudaErrors(cudaMallocArray(&mTransferFuncArray, &channelDesc2, sizeof(transferFunc)/sizeof(float4), 1));
  checkCudaErrors(cudaMemcpyToArray(mTransferFuncArray, 0, 0, transferFunc, sizeof(transferFunc), cudaMemcpyHostToDevice));

  mTransferTex.filterMode = cudaFilterModeLinear;
  mTransferTex.normalized = true;    // access with normalized texture coordinates
  mTransferTex.addressMode[0] = cudaAddressModeClamp;   // wrap texture coordinates

  // Bind the array to the texture
  checkCudaErrors(cudaBindTextureToArray(mTransferTex, mTransferFuncArray, channelDesc2));

  // Allocate Device Reference Buffer
  checkCudaErrors(cudaMallocManaged(&mDeviceReferenceBuffer, width*height*sizeof(uint)));
  checkCudaErrors(cudaMemset(mDeviceReferenceBuffer, 0, width*height*sizeof(uint)));
  // Allocate Device Render Buffer
  checkCudaErrors(cudaMallocManaged(&mDeviceRenderBuffer, width*height*sizeof(uint)));
  checkCudaErrors(cudaMemset(mDeviceRenderBuffer, 0, width*height*sizeof(uint)));
  // Allocate Buffer for Frame Differences
  checkCudaErrors(cudaMallocManaged(&mDeviceDiffBuffer, width*height*sizeof(uint)));
  checkCudaErrors(cudaMemset(mDeviceDiffBuffer, 0, width*height*sizeof(uint)));
}

void VolumeRendererCUDA::freeHostResources()
{
  hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Free up host resources.");
  delete [] mHostSiteMaskData;
  delete [] mHostVoxelData;
}

void VolumeRendererCUDA::freeResources()
{
  hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Free up GPU resources.");
  checkCudaErrors(cudaFreeArray(mVolumeArray));
  checkCudaErrors(cudaFreeArray(mTransferFuncArray));
  checkCudaErrors(cudaFreeArray(mSiteMask));
  checkCudaErrors(cudaFree(mDeviceRenderBuffer));
  checkCudaErrors(cudaFree(mDeviceReferenceBuffer));
  checkCudaErrors(cudaFree(mDeviceDiffBuffer));
}

void VolumeRendererCUDA::swapbuffer(uint** lhs, uint** rhs)
{
  *lhs = (uint*)((uintptr_t)(*lhs) ^ (uintptr_t)(*rhs));
  *rhs = (uint*)((uintptr_t)(*rhs) ^ (uintptr_t)(*lhs));
  *lhs = (uint*)((uintptr_t)(*lhs) ^ (uintptr_t)(*rhs));
}

void VolumeRendererCUDA::SlavePreRender(int rank, unsigned long startIteration)
{
  std::vector<size_t> indexStore;
  std::vector<float> valueStore;

  double startTimer = hemelb::util::myClock();
  size_t bytesSent = 0;

  mDataSource->Reset();

  while(mDataSource->ReadNext())
  {
    hemelb::util::Vector3D<size_t> sitePos = mDataSource->GetPosition();
    hemelb::util::Vector3D<double> siteVel = mDataSource->GetVelocity();
    double sitePressure = mDataSource->GetPressure();
    double velMagnitude = siteVel.GetMagnitude();
    double siteShearStress = mDataSource->GetShearStress();

    size_t index = (sitePos.z*mSystemSize + sitePos.y)*mSystemSize + sitePos.x;

    indexStore.push_back(index);
    valueStore.push_back((float)velMagnitude);
    valueStore.push_back((float)siteShearStress);
    valueStore.push_back(0.0f);
    valueStore.push_back((float)sitePressure);
  }

  int size = indexStore.size();
  mCommunicator->Send(size, rank);
  //size = valueStore.size();
  //mCommunicator->Send(size, rank);

  mCommunicator->Send(indexStore, rank);
  mCommunicator->Send(valueStore, rank);

  if(startIteration%100 == 0)
  {
    bytesSent = indexStore.size()*sizeof(size_t) + valueStore.size()*sizeof(float);
    double SendTime = hemelb::util::myClock() - startTimer;
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Slave %d sent %d bytes site data in %.4f seconds.", mCommunicator->Rank(), bytesSent, SendTime);
  }
}

void VolumeRendererCUDA::MasterPreRender(unsigned long startIteration)
{
  int nprocs = mCommunicator->Size();

  double startTimer = hemelb::util::myClock();

  size_t bytesReceived = 0;

  if(nprocs > 1)
  {
    for(int i = 1; i < nprocs; i++)
    {
      int expected = 0;

      mCommunicator->Receive(expected, i);
      std::vector<size_t> indexStore(expected);

      //mCommunicator->Receive(expected, i);
      std::vector<float> valueStore(expected*4);

      mCommunicator->Receive(indexStore, i);
      mCommunicator->Receive(valueStore, i);

      for(int j = 0; j < indexStore.size(); j++)
      {
        int index = indexStore[j];
        mHostVoxelData[index*4] = valueStore[j*4];
        mHostVoxelData[index*4+1] = valueStore[j*4+1];
        mHostVoxelData[index*4+2] = valueStore[j*4+2];
        mHostVoxelData[index*4+3] = valueStore[j*4+3];
      }

      bytesReceived += (indexStore.size()*sizeof(size_t) + valueStore.size()*sizeof(float));
    }
  }

  if(startIteration%100 == 0)
  {
    double SendTime = hemelb::util::myClock() - startTimer;
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Master receive %d bytes site data in %.4f seconds.", bytesReceived, SendTime);
  }

  mIterations = startIteration;

  mDataSource->Reset();

  while(mDataSource->ReadNext())
  {
    hemelb::util::Vector3D<size_t> sitePos = mDataSource->GetPosition();
    hemelb::util::Vector3D<double> siteVel = mDataSource->GetVelocity();
    double sitePressure = mDataSource->GetPressure();
    double velMagnitude = siteVel.GetMagnitude();
    double siteShearStress = mDataSource->GetShearStress();


    size_t index = (sitePos.z*mSystemSize + sitePos.y)*mSystemSize + sitePos.x;

    float* voxelData = &(mHostVoxelData[4*index]);
    *voxelData = (float)velMagnitude;
    *(voxelData+1) = (float)siteShearStress;
    *(voxelData+2) = 0.0f;
    *(voxelData+3) = (float)sitePressure;
  }

}

void VolumeRendererCUDA::Render(unsigned int *target, float density, float brightness, float offset, float scale, int mode)
{
  double renderstart = hemelb::util::myClock();

  /*mDataSource->Reset();
  if(mDataSource->ReadNext())
  {
    range[4] = (float)(mDataSource->GetMinShearStress());
    range[5] = (float)(mDataSource->GetMaxShearStress());
    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("max shear %.4f min shear %.4f", range[4],range[5]);
  }*/

  CopyDataToArray(mHostVoxelData, range);


  checkCudaErrors(cudaMemset(mDeviceDiffBuffer, 0, mImageWidth*mImageHeight*sizeof(uint)));

  checkCudaErrors(cudaMemset(mDeviceRenderBuffer, 0, mImageWidth*mImageHeight*sizeof(uint)));

  volumeRender_kernel<<<mGridSize, mBlockSize>>>(mDeviceRenderBuffer, mImageWidth, mImageHeight, density, brightness, offset, scale, mode);

  imageDiff_kernel<<<mGridSize, mBlockSize>>>(mDeviceDiffBuffer, mImageWidth, mImageHeight, mDeviceReferenceBuffer, mDeviceRenderBuffer);

  checkCudaErrors(cudaDeviceSynchronize());

  if(target != NULL)
  {
    checkCudaErrors(cudaMemcpy(target, mDeviceRenderBuffer, mImageWidth*mImageHeight*sizeof(uint), cudaMemcpyDeviceToDevice));
  }

  writeImage(mDeviceDiffBuffer);

  swapbuffer(&mDeviceRenderBuffer, &mDeviceReferenceBuffer);

  if(mIterations % 100 == 0)
  {
    double rendertime = hemelb::util::myClock() - renderstart;

    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("GPU CUDA render kernel %.4f seconds.", rendertime);
  }
}

void VolumeRendererCUDA::writeImage(const unsigned int* buffer)
{
  char filename[64];

  snprintf(filename, 64, "%08li.dat", mIterations);

  std::string path = mImageDirectory+std::string(filename);

  std::FILE* outputfile = std::fopen(path.c_str(), "w");

  if(outputfile == NULL)
  {
    throw hemelb::Exception() << "Failed to open file '" <<filename<<"'";
  }

  for(int i = 0; i < mImageWidth*mImageHeight; i++ )
  {
    if(mDeviceDiffBuffer[i]!= 0)
    {
      std::fwrite(&i, sizeof(int), 1, outputfile);
      std::fwrite(&mDeviceRenderBuffer[i], sizeof(int), 1, outputfile);
    }
  }

  std::fclose(outputfile);
}

void VolumeRendererCUDA::CopyInvViewMatrix(float *invViewMatrix, size_t size)
{
  checkCudaErrors(cudaMemcpyToSymbol(kInvViewMatrix, invViewMatrix, size));
}

void VolumeRendererCUDA::CopyDataToArray(float *data, float *range)
{
  cudaMemcpy3DParms copyParams = {0};
  copyParams.srcPtr   = make_cudaPitchedPtr(data, mVolumeSize.width*sizeof(float4), mVolumeSize.width, mVolumeSize.height);
  copyParams.dstArray = mVolumeArray;
  copyParams.extent   = mVolumeSize;
  copyParams.kind     = cudaMemcpyHostToDevice;
  checkCudaErrors(cudaMemcpy3D(&copyParams));

  checkCudaErrors(cudaMemcpyToSymbol(kValueRange, range, sizeof(float4)));
}
