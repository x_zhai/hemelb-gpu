/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/*
    Volume rendering sample

    This sample loads a 3D volume from disk and displays it using
    ray marching and 3D textures.

    Note - this is intended to be an example of using 3D textures
    in CUDA, not an optimized volume renderer.

    Changes
    sgg 22/3/2010
    - updated to use texture for display instead of glDrawPixels.
    - changed to render from front-to-back rather than back-to-front.
*/

#include "net/mpi.h"
#include "net/IOCommunicator.h"
#include "configuration/CommandLine.h"
#include "SimulationMaster.h"


// OpenGL Graphics includes
#include <visgpu/helper_gl.h>
#if defined (__APPLE__) || defined(MACOSX)
  #pragma clang diagnostic ignored "-Wdeprecated-declarations"
  #include <GLUT/glut.h>
  #ifndef glutCloseFunc
  #define glutCloseFunc glutWMCloseFunc
  #endif
#else
#include <GL/freeglut.h>
#endif

// CUDA Runtime, Interop, and includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cuda_profiler_api.h>
#include <vector_types.h>
#include <vector_functions.h>
#include <driver_functions.h>

// Helper functions
#include <visgpu/helper_cuda.h>
#include <visgpu/helper_functions.h>
#include <visgpu/helper_timer.h>

typedef unsigned int uint;
typedef unsigned char uchar;

#define MAX_EPSILON_ERROR 5.00f
#define THRESHOLD         0.30f

enum VISPARAM
{
    VIS_VELOCITY = 0,
    VIS_PRESSURE = 1,
    VIS_SHEAR_STRESS = 2,
    VIS_NUM_MODE
};

VISPARAM visParam = VIS_NUM_MODE;

const char *sSDKsample = "HemeLB CUDA 3D Volume Render";

uint width = 1024, height = 1024;

float3 viewRotation;
float3 viewTranslation = make_float3(0.0, 0.0, -4.0f);
float invViewMatrix[12];

float density = 0.05f;
float brightness = 1.0f;
float transferOffset = -0.0f;
float transferScale = 1.0f;
bool linearFiltering = true;

GLuint pbo = 0;     // OpenGL pixel buffer object
GLuint tex = 0;     // OpenGL texture object
struct cudaGraphicsResource *cuda_pbo_resource; // CUDA Graphics Resource (to transfer PBO)

StopWatchInterface *timer = 0;

// Auto-Verification Code
int fpsCount = 0;        // FPS count for averaging
int fpsLimit = 1;        // FPS limit for sampling
unsigned int frameCount = 0;

VolumeRendererCUDA *pVolumeRenderer = NULL;

static SimulationMaster *pMaster = NULL;

#ifndef MAX
#define MAX(a,b) ((a > b) ? a : b)
#endif

extern "C" void setTextureFilterMode(bool bLinearFilter);

void initPixelBuffer();

void dirtyDrawBitmapString(int x, int y, const char* string);
void dirtyDrawBitmapString(float x, float y, const char* string);

void drawVISSingleView(VISPARAM mode);
void drawVISSInfoQuadrant(VISPARAM mode);

void computeFPS()
{
    frameCount++;
    fpsCount++;

    if (fpsCount == fpsLimit)
    {
        char fps[256];
        float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
        sprintf(fps, "HemeLB Volume Render: %3.1f fps", ifps);

        glutSetWindowTitle(fps);
        fpsCount = 0;

        fpsLimit = (int)MAX(1.f, ifps);
        sdkResetTimer(&timer);
    }
}

//This is a genuinely filthy way of rendering texts!
//In fact the whole GLUT is quick and dirty to use but...
void dirtyDrawBitmapString(int x, int y, const char* string)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, width, height, 0, -100.0, 100.0);

    glColor3f(1,1,1);
    glRasterPos2i(x,y);
    const char* character = string;

    while(*character != '\0')
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *character);
        character++;
    }
}

void dirtyDrawBitmapString(float x, float y, const char* string)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -100.0, 100.0);

    glColor3f(1,1,1);
    glRasterPos2f(x,y);
    const char* character = string;

    while(*character != '\0')
    {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *character);
        character++;
    }

}
// render image using CUDA
void render()
{
    pVolumeRenderer->CopyInvViewMatrix(invViewMatrix, sizeof(float4)*3);

    // map PBO to get CUDA device pointer
    uint *d_output;
    // map PBO to get CUDA device pointer
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));
    size_t num_bytes;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&d_output, &num_bytes,
                                                         cuda_pbo_resource));
   //printf("CUDA mapped PBO: May access %ld bytes\n", num_bytes);

    // clear image
    checkCudaErrors(cudaMemset(d_output, 0, width*height*4));

    // call CUDA kernel, writing results to PBO
    //render_kernel(gridSize, blockSize, d_output, width, height, density, brightness, transferOffset, transferScale);

    pVolumeRenderer->Render(d_output, density, brightness, transferOffset, transferScale, (int)visParam);

    getLastCudaError("kernel failed");

    checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));
}

void drawVISSingleView(VISPARAM mode)
{
  float uv[VIS_NUM_MODE+1][8] =
  {
   {0.5f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f, 1.0f, 0.5f},//Velocity quadrant
   {0.0f, 0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.5f, 1.0f},//pressure quadrant
   {0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 1.0f, 1.0f},//stress quadrant
   {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f}//multi quadrant
  };

  glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, 1.0, 0.0, 1.0, -100.0, 100.0);

  // draw textured quad for the colour map
  glEnable(GL_TEXTURE_2D);
  glBegin(GL_TRIANGLE_STRIP);
  glColor3f(1,1,1);
  glTexCoord2f(uv[mode][0], uv[mode][1]);
  glVertex2f(0, 0);
  glTexCoord2f(uv[mode][2], uv[mode][3]);
  glVertex2f(1, 0);
  glTexCoord2f(uv[mode][4], uv[mode][5]);
  glVertex2f(0, 1);
  glTexCoord2f(uv[mode][6], uv[mode][7]);
  glVertex2f(1, 1);
  glEnd();

  glDisable(GL_TEXTURE_2D);

  if(mode == VIS_NUM_MODE)
  {
    glColor3f(1,1,1);
    glBegin(GL_LINES);
    glVertex2f(0.0f, 0.5f);
    glVertex2f(1.0f, 0.5f);
    glVertex2f(.5f, 0.0f);
    glVertex2f(.5f, 1.0f);
    glEnd();
  }

  switch(mode)
  {
    case VIS_VELOCITY:
      dirtyDrawBitmapString(10, 30, "Velocity Magnitude");
      break;
    case VIS_PRESSURE:
      dirtyDrawBitmapString(10, 30, "Pressure");
      break;
    case VIS_SHEAR_STRESS:
      dirtyDrawBitmapString(10, 30, "Shear Stress");
      break;
    case VIS_NUM_MODE:
      dirtyDrawBitmapString(10, 30, "[2] Pressure");
      dirtyDrawBitmapString(10 + (int)(width>>1), 30, "[3] Shear Stress");
      dirtyDrawBitmapString(10 + (int)(width>>1), 30 + (int)(height>>1), "[1] Velocity (Magnitude)");
      break;
    default:
      break;
  }
}

void drawVISSInfoQuadrant(VISPARAM mode)
{
  glDisable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, 1.0, 0.0, 1.0, -100.0, 100.0);

  glMatrixMode(GL_MODELVIEW);
  if(mode < VIS_NUM_MODE)
  {
    glPushMatrix();
    glLoadIdentity();
    glScalef(0.07, 0.5,1.0);
    glBegin(GL_TRIANGLE_STRIP);

    glColor3f(0,0,1);
    glVertex2f(0.1, 0.1);
    glVertex2f(1, 0.1);

    glColor3f(0,1,1);
    glVertex2f(0.1, 0.3);
    glVertex2f(1, 0.3);

    glColor3f(0,1,0);
    glVertex2f(0.1, 0.5);
    glVertex2f(1, 0.5);

    glColor3f(1,1,0);
    glVertex2f(0.1, 0.7);
    glVertex2f(1, 0.7);

    glColor3f(1,0,0);
    glVertex2f(0.1, 0.9);
    glVertex2f(1, 0.9);
    glEnd();
    glPopMatrix();
  }

  switch(mode)
  {
    char value[32];
    case VIS_VELOCITY:
      sprintf(value, "Min: %.4fm/s", pVolumeRenderer->range[0]);
      dirtyDrawBitmapString(0.08f, 0.05f, value);
      sprintf(value, "Max: %.4fm/s", pVolumeRenderer->range[1]);
      dirtyDrawBitmapString(0.08f, 0.45f, value);
      break;
    case VIS_PRESSURE:
      sprintf(value, "Min: %.4fpa", pVolumeRenderer->range[2]);
      dirtyDrawBitmapString(0.08f, 0.05f, value);
      sprintf(value, "Max: %.4fpa", pVolumeRenderer->range[3]);
      dirtyDrawBitmapString(0.08f, 0.45f, value);
      break;
    case VIS_SHEAR_STRESS:
      sprintf(value, "Min: %.4fpa", pVolumeRenderer->range[4]);
      dirtyDrawBitmapString(0.08f, 0.05f, value);
      sprintf(value, "Max: %.4fpa", pVolumeRenderer->range[5]);
      dirtyDrawBitmapString(0.08f, 0.45f, value);
      break;
    case VIS_NUM_MODE:
      dirtyDrawBitmapString(10, ((int)height>>1) + 100, "Keyboard Options:");
      dirtyDrawBitmapString(10, ((int)height>>1) + 140, "Press 1: Display Velocity");
      dirtyDrawBitmapString(10, ((int)height>>1) + 180, "Press 2: Display Pressure");
      dirtyDrawBitmapString(10, ((int)height>>1) + 220, "Press 3: Display Shear Stress");
      dirtyDrawBitmapString(10, ((int)height>>1) + 260, "Press 4: Show Multiview");
          
      dirtyDrawBitmapString(10, ((int)height>>1) + 300, "Press '+' and '-': Change Density");
      dirtyDrawBitmapString(10, ((int)height>>1) + 340, "Press ']' and '[': Change Brightness");
      dirtyDrawBitmapString(10, ((int)height>>1) + 380, "Press ';' and ''': Modify Transfer Function Offset");
      dirtyDrawBitmapString(10, ((int)height>>1) + 420, "Press '.' and ',': Modify Transfer Function Scale");
          
    default:
      break;
  }
}

// display results using OpenGL (called by GLUT)
void display()
{
    sdkStartTimer(&timer);

    // use OpenGL to build view matrix
    GLfloat modelView[16];
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-.5, .5, -.5, .5, .1, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glRotatef(-viewRotation.x, 1.0, 0.0, 0.0);
    glRotatef(-viewRotation.y, 0.0, 1.0, 0.0);
    glTranslatef(-viewTranslation.x, -viewTranslation.y, -viewTranslation.z);
    glGetFloatv(GL_MODELVIEW_MATRIX, modelView);

    glBegin(GL_QUADS);

    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f(1.0, -1.0, -1.0);
    glVertex3f(1.0, 1.0, -1.0);
    glVertex3f(-1.0, 1.0, -1.0);

    glEnd();

    glPopMatrix();

    invViewMatrix[0] = modelView[0];
    invViewMatrix[1] = modelView[4];
    invViewMatrix[2] = modelView[8];
    invViewMatrix[3] = modelView[12];
    invViewMatrix[4] = modelView[1];
    invViewMatrix[5] = modelView[5];
    invViewMatrix[6] = modelView[9];
    invViewMatrix[7] = modelView[13];
    invViewMatrix[8] = modelView[2];
    invViewMatrix[9] = modelView[6];
    invViewMatrix[10] = modelView[10];
    invViewMatrix[11] = modelView[14];

    render();

    // display results
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // draw image from PBO
    glDisable(GL_DEPTH_TEST);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    // draw using texture
    drawVISSingleView(visParam);
    // copy from pbo to texture
    drawVISSInfoQuadrant(visParam);

    /*glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-.5, .5, -.5, .5, .1, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(viewTranslation.x, viewTranslation.y, viewTranslation.z);
    glRotatef(viewRotation.x, 1.0, 0.0, 0.0);
    glRotatef(viewRotation.y, 0.0, 1.0, 0.0);

    glBegin(GL_LINES);

    glColor3f(1,0,0);
    glVertex3f(-.0, -.0, .0);
    glVertex3f(1,0,0);

    glColor3f(0,1,0);
    glVertex3f(-.0, -.0, .0);
    glVertex3f(0,1,0);

    glColor3f(0,0,1);
    glVertex3f(-.0, -.0, .0);
    glVertex3f(0,0,1);

    glEnd();

    glPopMatrix();*/

    glutSwapBuffers();
    glutReportErrors();

    sdkStopTimer(&timer);

    computeFPS();
}

void idle()
{
  pMaster->DoOneIteration();

  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27:
            #if defined (__APPLE__) || defined(MACOSX)
                exit(EXIT_SUCCESS);
            #else
                glutDestroyWindow(glutGetWindow());
                return;
            #endif
            break;

        case 'f':
            //linearFiltering = !linearFiltering;
            //setTextureFilterMode(linearFiltering);
            break;

        case '+':
            density += 0.01f;
            break;

        case '-':
            density -= 0.01f;
            break;

        case ']':
            brightness += 0.1f;
            break;

        case '[':
            brightness -= 0.1f;
            break;

        case ';':
            transferOffset += 0.01f;
            break;

        case '\'':
            transferOffset -= 0.01f;
            break;

        case '.':
            transferScale += 0.01f;
            break;

        case ',':
            transferScale -= 0.01f;
            break;
        case '1':
            visParam = VIS_VELOCITY;
            break;
        case '2':
            visParam = VIS_PRESSURE;
            break;
        case '3':
            visParam = VIS_SHEAR_STRESS;
             break;
        case '4':
            visParam = VIS_NUM_MODE;
             break;
        default:
            break;
    }

    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("density = %.2f, brightness = %.2f, transferOffset = %.2f, transferScale = %.2f\n", density, brightness, transferOffset, transferScale);

    glutPostRedisplay();
}

int ox, oy;
int buttonState = 0;

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        buttonState  |= 1<<button;
    }
    else if (state == GLUT_UP)
    {
        buttonState = 0;
    }

    ox = x;
    oy = y;
    glutPostRedisplay();
}

void motion(int x, int y)
{
    float dx, dy;
    dx = (float)(x - ox);
    dy = (float)(y - oy);

    if (buttonState == 4)
    {
        // right = zoom
        viewTranslation.z += dy / 100.0f;
    }
    else if (buttonState == 2)
    {
        // middle = translate
        viewTranslation.x += dx / 100.0f;
        viewTranslation.y -= dy / 100.0f;
    }
    else if (buttonState == 1)
    {
        // left = rotate
        viewRotation.x += dy / 5.0f;
        viewRotation.y += dx / 5.0f;
    }

    ox = x;
    oy = y;
    glutPostRedisplay();
}

int iDivUp(int a, int b)
{
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

void reshape(int w, int h)
{
    width = w;
    height = h;
    initPixelBuffer();

    pVolumeRenderer->Resize(w,h);

    // calculate new grid size

    glViewport(0, 0, w, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -100.0, 100.0);
}

void cleanup()
{
    sdkDeleteTimer(&timer);

    delete pVolumeRenderer;

    if (pbo)
    {
        cudaGraphicsUnregisterResource(cuda_pbo_resource);
        glDeleteBuffers(1, &pbo);
        glDeleteTextures(1, &tex);
    }

    // Calling cudaProfilerStop causes all profile data to be
    // flushed before the application exits
    checkCudaErrors(cudaProfilerStop());

    hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Shutting down");
    hemelb::net::MpiEnvironment::Abort(0);
}

void initGL(int *argc, char **argv)
{
    // initialize GLUT callback functions
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA volume rendering");

    if (!isGLVersionSupported(2,0) ||
        !areGLExtensionsSupported("GL_ARB_pixel_buffer_object"))
    {
        hemelb::log::Logger::Log<hemelb::log::Critical, hemelb::log::OnePerCore>("Visualisation node must have the required OpenGL capabilities.");
        exit(EXIT_SUCCESS);
    }

    cudaDeviceProp devProp = {0};
    checkCudaErrors(cudaGetDeviceProperties(&devProp, 0));

    if(devProp.unifiedAddressing)
    {
      hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("CUDA Device Supports: Unified Addressing.");
    }
}

void initPixelBuffer()
{
    if (pbo)
    {
        // unregister this buffer object from CUDA C
        checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource));

        // delete old buffer
        glDeleteBuffers(1, &pbo);
        glDeleteTextures(1, &tex);
    }

    // create pixel buffer object for display
    glGenBuffers(1, &pbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    // register this buffer object with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource, pbo, cudaGraphicsMapFlagsWriteDiscard));

    // create texture for display
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int
main(int argc, char **argv)
{
	hemelb::net::MpiEnvironment mpi(argc, argv);
	hemelb::log::Logger::Init();
	try
	{
		hemelb::net::MpiCommunicator commWorld = hemelb::net::MpiCommunicator::World();

		hemelb::net::IOCommunicator hemelbCommunicator(commWorld);
		try
		{
			// Parse command line
			hemelb::configuration::CommandLine options = hemelb::configuration::CommandLine(argc, argv);

			// Start the debugger (if requested)
			hemelb::debug::Debugger::Init(options.GetDebug(), argv[0], commWorld);

			// Prepare main simulation object...
			SimulationMaster master = SimulationMaster(options, hemelbCommunicator);

			// ..and run it.
      if( master.IsCurrentProcTheIOProc() )
      {
        hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Master");
        pMaster = &master;

        pVolumeRenderer = pMaster->GetVisualisationCtrl()->mVolRenCUDA;

        pVolumeRenderer->SetImageDirectory(options.GetOutputDir());

#if defined(__linux__)
        setenv ("DISPLAY", ":0", 0);
#endif

        //start logs
        hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("%s Starting...\n\n", sSDKsample);

        // First initialize OpenGL context, so we can properly set the GL for CUDA.
        // This is necessary in order to achieve optimal performance with OpenGL/CUDA interop.
        initGL(&argc, argv);

        // parse arguments
        sdkCreateTimer(&timer);

        hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Press '+' and '-' to change density (0.01 increments)\n"
               "      ']' and '[' to change brightness\n"
               "      ';' and ''' to modify transfer function offset\n"
               "      '.' and ',' to modify transfer function scale\n\n");

        // This is the normal rendering path for VolumeRender
        glutDisplayFunc(display);
        glutKeyboardFunc(keyboard);
        glutMouseFunc(mouse);
        glutMotionFunc(motion);
        glutReshapeFunc(reshape);
        glutIdleFunc(idle);

        initPixelBuffer();

#if defined (__APPLE__) || defined(MACOSX)
        atexit(cleanup);
#else
        glutCloseFunc(cleanup);
#endif
        glutMainLoop();
      }
      else
      {
        hemelb::log::Logger::Log<hemelb::log::Info, hemelb::log::OnePerCore>("Slave");
			  master.RunSimulation();
        master.Finalise();
      }
    }
		// Interpose this catch to print usage before propagating the error.
    catch (hemelb::configuration::CommandLine::OptionError& e)
		{
			hemelb::log::Logger::Log<hemelb::log::Critical, hemelb::log::Singleton>(hemelb::configuration::CommandLine::GetUsage());
		throw;
		}
	}
	catch (std::exception& e)
	{
		hemelb::log::Logger::Log<hemelb::log::Critical, hemelb::log::OnePerCore>(e.what());
		mpi.Abort(0);
	}
	// MPI gets finalised by MpiEnv's d'tor.
}
