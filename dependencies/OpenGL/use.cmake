hemelb_dependency(OpenGL find)

macro(hemelb_add_target_dependency_cuda tgt)
	target_include_directories(${tgt} PRIVATE ${CUDA_INCLUDE_DIRS})
	target_link_libraries(${tgt} PRIVATE ${CUDA_LIBRARIES})
endmacro()
