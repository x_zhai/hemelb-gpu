HemeLB is a massively parallel lattice-Boltzmann simulation software which is designed to provide the radiologist with estimates of flow rates, 
pressures and shear stresses throughout the relevant vascular structures, intended to eventually permit greater precision in the choice of therapeutic 
intervention. 

This is a version of Hemelb that optimised for GPU acceleration, it can be run directly on CUDA enabled devices. It has been tested on Zynq SoC, Jetson TX1 and desktop GPU PC.

The orignial version of hemelb can be found at: https://github.com/hemelb-codes/hemelb

If you find this code useful in your research, please consider citing:

    @ARTICLE{8918263,
	  author={X. {Zhai} and M. {Chen} and S. S. {Esfahani} and A. {Amira} and F. {Bensaali} and J. {Abinahed} and S. {Dakua} and R. A. {Richardson} and P. V. {Coveney}},
	  journal={IEEE Systems Journal}, 
	  title={Heterogeneous System-on-Chip-Based Lattice-Boltzmann Visual Simulation System}, 
	  year={2020},
	  volume={14},
	  number={2},
	  pages={1592-1601},
	  doi={10.1109/JSYST.2019.2952459}}

	@article{esfahani2020lattice,
	  title={Lattice-Boltzmann interactive blood flow simulation pipeline},
	  author={Esfahani, Sahar S and Zhai, Xiaojun and Chen, Minsi and Amira, Abbes and Bensaali, Faycal and AbiNahed, Julien and Dakua, Sarada and Younes, Georges and Baobeid, Abdulla and Richardson, Robin A and others},
	  journal={International journal of computer assisted radiology and surgery},
	  volume={15},
	  number={4},
	  pages={629--639},
	  year={2020},
	  publisher={Springer}
	}
